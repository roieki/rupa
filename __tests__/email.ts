import request from "supertest";

import app from "../src/app";

const validPayload = {
  to: "roie.cohen@gmail.com",
  to_name: "Roie Schwaber-Cohen",
  from: "roie.cohen@gmail.com",
  from_name: "Roie Schwaber-Cohen",
  subject: "Hello, World!",
  body: "<html><head><title>Hello, World!</title></head><body><h1>What's Up?</h1><p>Have a great day.</p></body></html>",
};

const invalidPayload = {
  to: "not-an-email",
  from: "not-an-email",
};

describe("Test app.ts", () => {
  test("Mailgun should send email successfully", async () => {
    process.env.ACTIVE_EMAIL_PROVIDER = "MAILGUN";
    const res = await request(app).post("/email").send(validPayload);
    expect(res.status).toEqual(200);
  });

  test("Sendgrid should send email successfully", async () => {
    process.env.ACTIVE_EMAIL_PROVIDER = "SENDGRID";
    const res = await request(app).post("/email").send(validPayload);
    expect(res.status).toEqual(200);
  });

  test("Invalid payload should return a 400", async () => {
    process.env.ACTIVE_EMAIL_PROVIDER = "SENDGRID";
    const res = await request(app).post("/email").send(invalidPayload);
    expect(res.status).toEqual(400);
  });

  test("Invalid provider should return a 500", async () => {
    process.env.ACTIVE_EMAIL_PROVIDER = "INVALID";
    const res = await request(app).post("/email").send(validPayload);
    expect(res.status).toEqual(500);
  });

  test("Bad Mailgun API key should return a 500", async () => {
    process.env.MAILGUN_API_KEY = "BAD_API_KEY";
    const res = await request(app).post("/email").send(validPayload);
    expect(res.status).toEqual(500);
  });

  test("Bad SendGrid API key should return a 500", async () => {
    process.env.ACTIVE_EMAIL_PROVIDER = "SENDGRID";
    process.env.SENDGRID_API_KEY = "BAD_API_KEY";
    const res = await request(app).post("/email").send(validPayload);
    expect(res.status).toEqual(500);
  });

  test("Bad MailGun domain key should return a 500", async () => {
    process.env.ACTIVE_EMAIL_PROVIDER = "MAILGUN";
    process.env.MAILGUN_DOMAIN = "BAD_DOMAIN";
    const res = await request(app).post("/email").send(validPayload);
    expect(res.status).toEqual(500);
  });
});
