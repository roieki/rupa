import { Email } from "../types";

export abstract class EmailService {
  abstract getHeaders(): HeadersInit;
  abstract getEmailProviderUrl(): string;
  abstract getBody(email: Email): string;
  public async sendEmail(email: Email) {
    try {
      return await fetch(this.getEmailProviderUrl(), {
        method: "post",
        body: this.getBody(email),
        headers: this.getHeaders(),
        redirect: "follow",
      });
    } catch (e) {
      throw new Error(`Unable to send email : ${e}`);
    }
  }
}

class SendGrid extends EmailService {
  public getHeaders(): HeadersInit {
    return {
      "Content-Type": "application/json",
      Authorization: `Bearer ${process.env.SENDGRID_API_KEY}`,
    };
  }
  public getEmailProviderUrl(): string {
    return "https://api.sendgrid.com/v3/mail/send";
  }
  public getBody(email: Email): string {
    return JSON.stringify({
      personalizations: [
        {
          to: [
            {
              email: email.to,
              name: email.to_name,
            },
          ],
          subject: email.subject,
        },
      ],
      from: {
        email: email.from,
        name: email.from_name,
      },
      content: [
        {
          type: "text/plain",
          value: email.body,
        },
      ],
    });
  }
}

class MailGun extends EmailService {
  public getHeaders(): HeadersInit {
    return {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${Buffer.from(
        `api:${process.env.MAILGUN_API_KEY}`
      ).toString("base64")}`,
    };
  }
  public getEmailProviderUrl(): string {
    return `https://api.mailgun.net/v3/${process.env.MAILGUN_DOMAIN}/messages`;
  }
  public getBody(email: Email): string {
    return new URLSearchParams({
      from: `${email.from_name} <${email.from}>`,
      to: `${email.to_name} <${email.to}>`,
      subject: email.subject,
      text: email.body,
    }).toString();
  }
}

export enum SupportedEmailProviders {
  SENDGRID = "SENDGRID",
  MAILGUN = "MAILGUN",
}

export class EmailServiceFactory {
  static getEmailService(serviceName: SupportedEmailProviders): EmailService {
    switch (serviceName) {
      case SupportedEmailProviders.SENDGRID:
        return new SendGrid();
      case SupportedEmailProviders.MAILGUN:
        return new MailGun();
      default:
        throw new Error("Invalid email service");
    }
  }
}
