import express, { Application, Request, Response, NextFunction } from "express";
import bodyParser from "body-parser";
import * as dotenv from "dotenv";
import { router as emailRouter } from "./routes/email";

dotenv.config();

const app = express();
const jsonParser = bodyParser.json();

app.use(jsonParser);
app.use("/email", emailRouter);

export default app;
