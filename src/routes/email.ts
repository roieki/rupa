import { Router, Request, Response } from "express";
import { body, validationResult } from "express-validator";
import { convert } from "html-to-text";
import { Email } from "../types";
import {
  EmailService,
  EmailServiceFactory,
  SupportedEmailProviders,
} from "../classes/emailServiceFactory";

const router = Router();

router.post(
  "/",
  body("to").isEmail(),
  body("to_name").exists(),
  body("from").isEmail(),
  body("from_name").exists(),
  body("subject").exists(),
  body("body").exists(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ message: "Validation failed", errors: errors.array() });
    }

    const email: Email = req.body as Email;

    //Convert HTML to text
    email.body = convert(email.body);

    try {
      const activeEmailProvider =
        (process.env.ACTIVE_EMAIL_PROVIDER as SupportedEmailProviders) ??
        SupportedEmailProviders.SENDGRID;

      const emailer: EmailService =
        EmailServiceFactory.getEmailService(activeEmailProvider);

      const result = await emailer.sendEmail(email);

      if (result?.status === 200 || result?.status === 202) {
        res
          .status(200)
          .send(`Email sent successfully using ${activeEmailProvider}`);
      } else {
        res
          .status(500)
          .send(`Sending email failed to using ${activeEmailProvider}`);
      }
    } catch (e) {
      res.status(500).send(`Sending email failed: ${e}`);
    }
  }
);

export { router };
