# Readme

## Prerequisites
You must be using Node.js 18 or higher. This project uses `fetch` which is native in Node.js 18 and higher (you can use `nvm install 18` to install Node.js 18).

## Installation
1. To install the dependencies, run `npm install`.
2. You must obtain the API key and domain for Mailgun as well as the API key for SendGrid. You can obtain these by creating a free account on their respective websites.
3. Rename the `.env.example` file to `.env` and fill in the values for the API keys and domain.
4. Set the `ACTIVE_MAIL_PROVIDER` environment variable to either `MAILGUN` or `SENDGRID` depending on which provider you want to use. For example:

```
SENDGRID_API_KEY=<Your Sendgrid API key>
MAILGUN_DOMAIN=<Your Mailgun Domain>
MAILGUN_API_KEY=<Your Mailgun API key>
ACTIVE_EMAIL_PROVIDER=MAILGUN
PORT=3000
```

## Running the app
To run the app, run `npm run dev`.

## Language, Framework and Libraries
Generally speaking, I've used technologies and libraries that are popular and widely used in the industry. I did so because I believe that it is important to use technologies that are well supported and have a large community behind them. This ensure that overall the code is easier to debug and maintain.

1. I chose to use **Node.js** for this project because I'm the most familiar with it. Additionally, since Javascript/Typescript is considered to be one the most popular languages in the world, I thought other developers should be able to easily understand the code. I use **Typescript** for similar reasons, but also because I believe type safety makes it easier to write and debug code. It also makes it easier to produce higher quality code with less bugs.
2. I chose to use **Express.js** to handle routing in the app, both for its popularity and because it's long been a standard for Node.js web apps. Other developers should be familiar with it and it's easy to use. I used `body-parser` to parse the request body.
3. I use the native `fetch` capability that is native in Node.js 18 to handle the API calls. I believe it is preferable to use native capabilities over libraries when possible.
4. `express-validator` is used to validate the input from the user. It's a popular library which makes it easy to validate input and return errors to the user. It is well documented and maintained.
5. `http-to-text` is used to convert the HTML body sent to the server to text. It is a small library which seems well maintained and is easy to use.
6. I use `jest` and `supertest` for testing. I chose `jest` because it is the most popular testing framework for Javascript and `supertest` because it is the most popular library for testing Express.js apps.
7. To enable developing with Typescript, I use `ts-node-dev` to run the app in development mode and `ts-node` to rnu the app in production mode.
8. `dotenv` is used to load environment variables from the `.env` file.

## Tradeoffs
1. I would probably try to make the validation a bit more generic and less specific to the requirements of this project. It is currently tightly coupled to the router and should be pulled out into a separate module.
2. I'd like to improve the responses from the server when the email is sent successfully and add a provider specific handler for the response. Currently, the response is just a generic success message.

## Time spent
I spent about 4 hours on this project.
